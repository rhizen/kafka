package kafka

import (
	kfk "github.com/dropbox/kafka"
	"github.com/dropbox/kafka/proto"
)

type Broker struct {
	broker *kfk.Broker
	conf   *Config
}
type Config struct {
	clientId      string
	cluster string
	partition int32
	startOffset int64
}

func NewBroker(addr []string, conf *Config) (*Broker, error) {
	c := kfk.NewBrokerConf(conf.clientId)
	c.AllowTopicCreation = true
	broker, err := kfk.NewBroker(conf.cluster, addr, c)

	return &Broker{broker, conf}, err
}

func (b *Broker) NewProducer() kfk.Producer {
	c := kfk.NewProducerConf()
	c.Compression = proto.CompressionGzip
	c.RequiredAcks = proto.RequiredAcksLocal

	return b.broker.Producer(c)
}

func (b *Broker) NewConsumer(topic string) (kfk.Consumer, error) {
	c := kfk.NewConsumerConf(topic, b.conf.partition)
	c.StartOffset = b.conf.startOffset

	return b.broker.Consumer(c)
}

func NewMessage(m string) *proto.Message {
	return &proto.Message{Value: []byte(m)}
}