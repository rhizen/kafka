package kafka

import (
	"github.com/Shopify/sarama"
	"github.com/pkg/errors"
	"fmt"
	"github.com/Sirupsen/logrus"
	"os"
	"strings"
	"time"
)

type Producer struct {
	sp    sarama.SyncProducer
	logger *logrus.Logger
	topic string
}

func NewProducer(brokerList []string, topic string) (*Producer, error) {

	// For the data collector, we are looking for strong consistency semantics.
	// Because we don't change the flush settings, sarama will try to produce messages
	// as fast as possible to keep latency low.
	config := sarama.NewConfig()
	config.Producer.RequiredAcks = sarama.WaitForAll // Wait for all in-sync replicas to ack the message
	config.Producer.Retry.Max = 10                   // Retry up to 10 times to produce the message
	config.Producer.Return.Successes = true
	//tlsConfig := createTlsConfiguration()
	//if tlsConfig != nil {
	//  config.Net.TLS.Config = tlsConfig
	//  config.Net.TLS.Enable = true
	//}

	l := newLogger()
	// On the broker side, you may want to change the following settings to get
	// stronger consistency guarantees:
	// - For your broker, set `unclean.leader.election.enable` to false
	// - For the topic, you could increase `min.insync.replicas`.

	p, err := sarama.NewSyncProducer(brokerList, config)
	if err != nil {
		l.WithFields(logrus.Fields{
			"brokers": strings.Join(brokerList, ","),
		}).Warn(err)
		//return nil, err
	}

	return &Producer{
		sp: p,
		topic: topic,
		logger: newLogger(),
	}, nil
}

func newLogger() *logrus.Logger {
	var log = logrus.New()

	os.Mkdir("logs", 0755)
	file, err := os.OpenFile("logs/kafka.log", os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0664)
	if err != nil {
		logrus.Fatal("open log file failed.")
	}
	log.Out = file
	return log
}

func (p *Producer) Close() error {
	return p.sp.Close()
}

func (p *Producer) LogToFile(topic, message string) error {
	p.logger.WithFields(logrus.Fields{
		"topic": topic,
		"time": time.Now(),
	}).Info(message)
	return nil
}

func (p *Producer) SendMessage(message string) error {
	if p.sp == nil {
		return p.LogToFile(p.topic, message)
	}

	partition, offset, err := p.sp.SendMessage(
		&sarama.ProducerMessage{
			Topic: p.topic,
			Value: sarama.StringEncoder(message),
		})

	if err != nil {
		return errors.Wrap(err, fmt.Sprintf("failed store data: %d, %d", partition, offset))
	}

	return nil
}

//func createTlsConfiguration(certFile, keyFile, caFile string) (t *tls.Config, error) {
//  if certFile != "" && keyFile != "" && caFile != "" {
//    cert, err := tls.LoadX509KeyPair(certFile, keyFile)
//    if err != nil {
//      return nil, err
//    }
//
//    caCert, err := ioutil.ReadFile(caFile)
//    if err != nil {
//      return nil, err
//    }
//
//    caCertPool := x509.NewCertPool()
//    caCertPool.AppendCertsFromPEM(caCert)
//
//    t = &tls.Config{
//      Certificates:       []tls.Certificate{cert},
//      RootCAs:            caCertPool,
//      InsecureSkipVerify: *verifySsl,
//    }
//  }
//  // will be nil by default if nothing is provided
//  return t
//}

