package kafka

import (
	"testing"
	kfk "github.com/dropbox/kafka"
	"github.com/stretchr/testify/assert"
	"time"
)

var addr = []string{"192.168.10.24:9094"}
var config = &Config{
	clientId: "192.168.10.24",
	cluster: "test-cluster",
	partition: 0,
	startOffset: kfk.StartOffsetNewest,
}

func TestProducerAndConsumer(t *testing.T) {
	broker, err := NewBroker(addr, config)
	assert.NoError(t, err, "should not get error")

	start := time.Now()
	message := "produce message"
	topic := "test"

	go func() {
		c, err := broker.NewConsumer(topic)
		assert.NoError(t, err, "should not get error")

		for {
			msg, err := c.Consume()
			if err != nil {
				if err != kfk.ErrNoData {
					t.Error(err)
				}
				break
			}
			assert.Equal(t, message, string(msg.Value), "should be same")
			break
		}
	}()

	producer := broker.NewProducer()
	offset, err := producer.Produce(topic, 0, NewMessage(message))
	assert.NoError(t, err, "should not get error")
	comp := func() bool {
		return offset >= 0
	}
	assert.Condition(t, comp, "should larger than 0")

	expires := 1*time.Second
	assert.WithinDuration(t, start.Add(expires), time.Now(), expires, "too long, should not be longer than 1 second")
}

func BenchmarkProduce(b *testing.B) {
	broker, _ := NewBroker(addr, config)
	producer := broker.NewProducer()
	message := "produce message"
	topic := "test"
	for i := 0;i < b.N; i ++ {
		producer.Produce(topic, 0, NewMessage(message))
	}
}